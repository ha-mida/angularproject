FROM node:14.21.2 as builder
 
Run mkdir -p /app

WORKDIR /app

COPY package.json /app/
RUN npm install

COPY .  .
RUN  npm run build --prod


#stage 2
FROM nginx:alpine
COPY --from=builder /app/dist/test   /usr/share/nginx/html


